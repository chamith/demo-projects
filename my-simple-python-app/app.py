#! /usr/bin/python

from flask import Flask
from flask_restful import Resource, Api

import sys
import optparse
import time
import mysql.connector
import json
import sqlite3

app = Flask(__name__)

start = int(round(time.time()))

@app.route("/")
def hello_world():
#	mydb = mysql.connector.connect(
#		host="mysql",
#		user="root",
#		passwd="password",
#		database="learnright"
#	)

	#cursor = mydb.cursor()
    #cursor.execute('SELECT * FROM student')
    #results = [{id: name} for (id, name) in cursor]
    #cursor.close()
    #connection.close()
    #return json.dumps({'students': results})
    return "hello"

@app.route("/students/add")
def check_db():
	conn = sqlite3.connect('simple-app.db')
	c = conn.cursor()
	c.execute("CREATE TABLE student (id int, name text)")
	conn.commit()
	conn.close()
	return "table created"


if __name__ == '__main__':
    parser = optparse.OptionParser(usage="python simpleapp.py -p ")
    parser.add_option('-p', '--port', action='store', dest='port', help='The port to listen on.')
    (args, _) = parser.parse_args()
    if args.port == None:
        print "Missing required argument: -p/--port"
        sys.exit(1)
    app.run(host='0.0.0.0', port=int(args.port), debug=False)
