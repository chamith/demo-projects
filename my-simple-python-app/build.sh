#! /bin/bash
kubectl delete deployments my-simple-python-app-deployment
docker build . -t localhost:32000/my-simple-python-app:registry
docker push localhost:32000/my-simple-python-app
kubectl apply -f deployment.yaml