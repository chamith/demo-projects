package payroll;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public class EmployeeControllerTests {

    @Autowired
    private EmployeeController cont;
 
    @MockBean
    private EmployeeRepository repo;
    
    @Before
	public void setUp() {
	    Employee alex = new Employee("chamith", "director");
	 
	    Mockito.when(repo.getOne((long) 1)).thenReturn(alex);
	}
	
	@Test
	public void whenFindByName_thenReturnEmployee() {

	    // when
	    Employee found = repo.getOne((long) 1);
	 
	    
	}
}
