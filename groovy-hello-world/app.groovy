@RestController
class ThisWillActuallyRun {

	@RequestMapping("/")
	String home() {
		"Hello World!"
	}

	@RequestMapping("/version")
	String version() {
		GroovySystem.version
	}	

}

